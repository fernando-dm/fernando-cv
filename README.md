# Dillinger

# Cv de Fernando García

En este repo, uso resume [jsonresume](https://jsonresume.org/), que es una app de node que en base a un json con mis datos, te arma un hermoso html u pdf. Tambien le puse IC para que en un futuro (lejano), pueda actualizar sin problemas y mantener un cv al día.

Instalacion de resume:

De forma global:

```	
npm install -g resume-cli
```

O local! (con las librerias de node en el mismo path, de ser asi, armar un gitiganore y no subirlas)
```	
npm install resume-cli
```

Genero package.json

```
	npm init -f
```

Editamos package.json , agregando una linea dentro de la sección scripts :

```
"scripts": {
	"resume": "resume"
	}

// Este ultimo para frizar versiones
"dependencies": {
    "jsonresume-theme-elegant": "^1.13.2",
    "jsonresume-theme-kendall": "^0.2.0",
    "resume-cli": "^2.0.0"
  }
```
 - Esta sección agrega un script npm llamado resume. 
 - Con este script agregado podemos utilizar: npm run resume -- -h para ver la ayuda. 
 - Notar que el – significa que lo que siga son argumentos del script resume, y no npm.

Por último, acceder al CV mediante el siguiente link: https://fernando-dm.gitlab.io/fernando-cv/
